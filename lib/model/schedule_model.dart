import 'package:equatable/equatable.dart';

class ScheduleModel extends Equatable {
  final String? day;
  final String? month;
  final String? year;
  final String? time;

  ScheduleModel({
    required this.day,
    required this.month,
    required this.year,
    required this.time,
  });

  @override
  List<Object?> get props => [day, month, year, time];
}
