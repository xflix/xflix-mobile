// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'checkout_model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class CheckoutDB extends TypeAdapter<CheckoutModel> {
  @override
  final int typeId = 1;

  @override
  CheckoutModel read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return CheckoutModel(
      location: fields[0] as LocationModel?,
      movie: fields[1] as MovieModel?,
      schedule: fields[2] as ScheduleModel?,
      price: fields[4] as int,
      totalPrice: fields[5] as int,
      tax: fields[6] as int,
      seats: fields[3] as String,
    );
  }

  @override
  void write(BinaryWriter writer, CheckoutModel obj) {
    writer
      ..writeByte(7)
      ..writeByte(0)
      ..write(obj.location)
      ..writeByte(1)
      ..write(obj.movie)
      ..writeByte(2)
      ..write(obj.schedule)
      ..writeByte(3)
      ..write(obj.seats)
      ..writeByte(4)
      ..write(obj.price)
      ..writeByte(5)
      ..write(obj.totalPrice)
      ..writeByte(6)
      ..write(obj.tax);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CheckoutDB &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
