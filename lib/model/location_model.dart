import 'package:equatable/equatable.dart';
import 'package:hive/hive.dart';
part 'location_model.g.dart';

@HiveType(typeId: 3, adapterName: 'LocationDB')
class LocationModel extends Equatable {
  @HiveField(0)
  final String? id;
  @HiveField(1)
  final String? name;
  @HiveField(2)
  final String? place;
  final PositionModel? position;

  LocationModel({this.id, this.name, this.place, this.position});

  factory LocationModel.fromjson(Map<String, dynamic> json) {
    return LocationModel(
        id: json['uuid'],
        name: json['nama'],
        place: json['place'],
        position: PositionModel.fromjson(json['map']));
  }

  @override
  List<Object?> get props => [
        id,
        name,
        place,
        position,
      ];
}

class PositionModel {
  final String? lat;
  final String? lot;

  PositionModel({this.lat, this.lot});

  factory PositionModel.fromjson(List<dynamic> json) {
    return PositionModel(
      lat: json[0],
      lot: json[1],
    );
  }
}
