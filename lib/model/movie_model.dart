import 'package:hive/hive.dart';
part 'movie_model.g.dart';

@HiveType(typeId: 2, adapterName: 'MovieDB')
class MovieModel {
  @HiveField(1)
  final int id;
  @HiveField(2)
  final String? originalLanguage;
  @HiveField(3)
  final String? originalTitle;
  @HiveField(4)
  final String? posterPath;
  @HiveField(5)
  final String? backdropPath;
  @HiveField(6)
  final double voteAverage;
  @HiveField(7)
  final String? release;
  @HiveField(8)
  final int voteCount;
  @HiveField(9)
  final String? overview;

  MovieModel({
    required this.id,
    this.originalLanguage,
    this.originalTitle,
    this.overview,
    this.backdropPath,
    this.posterPath,
    this.release,
    required this.voteAverage,
    required this.voteCount,
  });

  factory MovieModel.fromjson(Map<String, dynamic> json) {
    double voteAverageValue;
    if (json['vote_average'] is int) {
      voteAverageValue = json['vote_average'].toDouble();
    } else {
      voteAverageValue = json['vote_average'];
    }
    return MovieModel(
      id: json['id'],
      originalLanguage: json['original_language'],
      originalTitle: json['original_title'],
      overview: json['overview'],
      posterPath: json['poster_path'],
      voteAverage: voteAverageValue,
      voteCount: json['vote_count'],
      backdropPath: json['backdrop_path'],
      release: json['release_date'],
    );
  }
}
