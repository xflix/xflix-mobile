import 'package:hive/hive.dart';
import 'package:movie_bloc/model/location_model.dart';
import 'package:movie_bloc/model/movie_model.dart';
import 'package:movie_bloc/model/schedule_model.dart';
part 'checkout_model.g.dart';

@HiveType(typeId: 1, adapterName: 'CheckoutDB')
class CheckoutModel {
  @HiveField(0)
  final LocationModel? location;
  @HiveField(1)
  final MovieModel? movie;
  @HiveField(2)
  final ScheduleModel? schedule;
  @HiveField(3)
  final String seats;
  @HiveField(4)
  final int price;
  @HiveField(5)
  final int totalPrice;
  @HiveField(6)
  final int tax;

  CheckoutModel({
    this.location,
    this.movie,
    this.schedule,
    this.price = 0,
    this.totalPrice = 0,
    this.tax = 0,
    this.seats = '',
  });
}
