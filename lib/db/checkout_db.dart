import 'package:hive/hive.dart';
import 'package:movie_bloc/model/checkout_model.dart';
import 'package:movie_bloc/model/location_model.dart';
import 'package:movie_bloc/model/movie_model.dart';

class CheckoutDatabase {
  String _boxName = 'CheckoutDB';

  Future<Box> checkoutBox() async {
    var box = await Hive.openBox<CheckoutModel>(_boxName);
    return box;
  }

  Future<CheckoutModel> getCheckOut() async {
    final box = await checkoutBox();
    CheckoutModel checkout = box.getAt(0);
    return checkout;
  }

  Future<void> addLocation(LocationModel locationModel) async {
    CheckoutModel checkoutModel = CheckoutModel(location: locationModel);
    final box = await checkoutBox();
    await box.add(checkoutModel);
  }

  Future<void> addMovie(MovieModel movieModel) async {}

  Future<int> getAllCheckOut() async {
    final box = await checkoutBox();
    int checks = box.values.length;
    return checks;
  }
}
