import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_bloc/bloc/movie/movie_bloc.dart';
import 'package:movie_bloc/ui/widget/horizontal_movie_card.dart';
import 'package:movie_bloc/util/theme.dart';

class ListHeaderImage extends StatelessWidget {
  const ListHeaderImage({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MovieBloc, MovieState>(
      builder: (context, state) {
        if (state is MovieSuccess) {
          return Container(
            height: 200,
            width: double.infinity,
            margin: EdgeInsets.only(
              top: 10,
            ),
            child: PageView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: 5,
              controller: PageController(
                initialPage: 2,
                viewportFraction: 0.9,
              ),
              itemBuilder: (context, index) {
                return HorizontalMovieCard(
                  height: 250,
                  movie: state.movies[index],
                );
              },
            ),
          );
        } else {
          return SizedBox(
            child: Text(
              'Failed Get Data',
              style: whiteTextStyle,
            ),
          );
        }
      },
    );
  }
}
