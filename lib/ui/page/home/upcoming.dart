import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_bloc/bloc/movie/movie_bloc.dart';
import 'package:movie_bloc/ui/widget/card_movie.dart';
import 'package:movie_bloc/util/theme.dart';

class UpComing extends StatelessWidget {
  const UpComing({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MovieBloc, MovieState>(
      builder: (context, state) {
        if (state is MovieSuccess) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.only(
                  top: 15,
                  left: defaultMargin,
                ),
                child: Text(
                  'UpComing',
                  style: darkTextStyle.copyWith(
                    fontSize: 18,
                    fontWeight: semiBold,
                  ),
                ),
              ),
              Container(
                height: 250,
                margin: EdgeInsets.only(top: 10),
                child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: state.commingMovies.length,
                    itemBuilder: (context, index) {
                      return CardMovie(movie: state.commingMovies[index]);
                    }),
              )
              // CardMovie()
            ],
          );
        } else {
          return SizedBox();
        }
      },
    );
  }
}
