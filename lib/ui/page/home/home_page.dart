import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:movie_bloc/bloc/movie/movie_bloc.dart';
import 'package:movie_bloc/ui/page/home/list_header_image.dart';
import 'package:movie_bloc/ui/page/home/nowplaying.dart';
import 'package:movie_bloc/ui/page/home/popular.dart';
import 'package:movie_bloc/ui/page/home/upcoming.dart';
import 'package:movie_bloc/util/theme.dart';

class HomePage extends StatefulWidget {
  static const String id = 'home_page';
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  //NOTE:Header List of Movie
  Widget header() {
    return ListHeaderImage();
  }

  //NOTE:Now Playing Momvie
  Widget nowplaying() {
    return Nowplaying();
  }

  //NOTE:Upcoming Movie
  Widget upComing() {
    return UpComing();
  }

  //NOTE:Popular Movie
  Widget popular() {
    return Popular();
  }

  @override
  void initState() {
    context.read<MovieBloc>().add(GetMovieTrending());
    super.initState();
  }

  Widget footer() {
    return SizedBox(
      height: 150,
    );
  }

//NOTE:Scaffold
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Discovery',
          style: darkTextStyle.copyWith(
            fontSize: 18,
            fontWeight: semiBold,
          ),
        ),
        backgroundColor: kWhiteColor,
        elevation: 0,
        actions: [
          Icon(
            FontAwesomeIcons.search,
            color: kPrimaryColor,
            size: 20,
          ),
          SizedBox(
            width: 10,
          ),
        ],
      ),
      backgroundColor: kWhiteColor,
      body: SafeArea(
          child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            header(),
            nowplaying(),
            upComing(),
            popular(),
            nowplaying(),
            upComing(),
            popular(),
            footer(),
          ],
        ),
      )),
    );
  }
}
