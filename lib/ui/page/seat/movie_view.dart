import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_bloc/bloc/order/order_bloc.dart';
import 'package:movie_bloc/util/theme.dart';

class MovieView extends StatelessWidget {
  const MovieView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<OrderBloc, OrderState>(
      builder: (context, state) {
        if (state is OrderSuccess) {
          return Container(
            margin: EdgeInsets.only(
                top: 15, right: defaultMargin, left: defaultMargin, bottom: 10),
            child: Text(
              state.movie.originalTitle.toString(),
              style: whiteTextStyle.copyWith(
                fontWeight: semiBold,
                fontSize: 20,
              ),
            ),
          );
        } else {
          return SizedBox();
        }
      },
    );
  }
}
