import 'package:flutter/material.dart';
import 'package:movie_bloc/ui/widget/status_item.dart';
import 'package:movie_bloc/util/theme.dart';

class StatusView extends StatelessWidget {
  const StatusView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        left: defaultMargin,
        right: defaultMargin,
        bottom: 20,
        top: 25,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          StatusItem(
            name: 'Available',
            color: kGreyColor,
          ),
          StatusItem(
            name: 'Selected',
            color: kGreenColor,
          ),
          StatusItem(
            name: 'Unavailable',
            color: kPinkColor,
          ),
        ],
      ),
    );
  }
}
