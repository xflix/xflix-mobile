import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_bloc/bloc/schedule_order/schedule_order_cubit.dart';
import 'package:movie_bloc/util/theme.dart';

class ScheduleView extends StatelessWidget {
  const ScheduleView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ScheduleOrderCubit, ScheduleOrderState>(
      builder: (context, state) {
        if (state is ScheduleOrderSuccess) {
          return Container(
            margin: EdgeInsets.symmetric(horizontal: defaultMargin),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Day : ${state.scheduleModel.day} / ${state.scheduleModel.month} / ${state.scheduleModel.year}",
                  style: whiteTextStyle.copyWith(
                      fontSize: 16, fontWeight: semiBold),
                ),
                Text(
                  "Time : ${state.scheduleModel.time} PM",
                  style: whiteTextStyle.copyWith(
                      fontSize: 16, fontWeight: semiBold),
                )
              ],
            ),
          );
        } else {
          return SizedBox();
        }
      },
    );
  }
}
