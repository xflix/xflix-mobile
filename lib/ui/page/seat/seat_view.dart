import 'package:flutter/material.dart';
import 'package:movie_bloc/ui/widget/card_seat.dart';
import 'package:movie_bloc/util/theme.dart';

class SeatView extends StatelessWidget {
  const SeatView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(
        horizontal: defaultMargin,
      ),
      child: GridView.count(
        crossAxisCount: 9,
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        children: List.generate(
          100,
          (index) {
            if ((index + 1) % (5) == 0 && index < (index + 1)) {
              return SizedBox(
                width: 10,
              );
            } else {
              if ((index + 1) % 2 == 0) {
                return CardSeat(
                  id: "A${index + 1}",
                  isAvailable: false,
                );
              } else {
                return CardSeat(
                  id: "A${index + 1}",
                );
              }
            }
          },
        ),
      ),
    );
  }
}
