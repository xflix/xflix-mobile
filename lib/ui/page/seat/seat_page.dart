import 'package:flutter/material.dart';
import 'package:movie_bloc/ui/page/payment/payment_page.dart';
import 'package:movie_bloc/ui/page/seat/location_view.dart';
import 'package:movie_bloc/ui/page/seat/movie_view.dart';
import 'package:movie_bloc/ui/page/seat/schedule_view.dart';
import 'package:movie_bloc/ui/page/seat/seat_view.dart';
import 'package:movie_bloc/ui/page/seat/status_view.dart';
import 'package:movie_bloc/ui/page/seat/total_view.dart';
import 'package:movie_bloc/ui/widget/custom_button.dart';
import 'package:movie_bloc/util/theme.dart';

class SeatPage extends StatelessWidget {
  static const String id = "seat_page";
  const SeatPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget movieView() {
      return MovieView();
    }

    Widget locationView() {
      return LocationView();
    }

    Widget scheduleView() {
      return ScheduleView();
    }

    Widget statusView() {
      return StatusView();
    }

    Widget seatView() {
      return SeatView();
    }

    Widget totalView() {
      return TotalView();
    }

    Widget buttonView() {
      return CustomButton(
          onpress: () {
            Navigator.pushNamed(context, PaymentPage.id);
          },
          name: "Confirm");
    }

    return Scaffold(
      backgroundColor: kPrimaryColor,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              movieView(),
              locationView(),
              scheduleView(),
              statusView(),
              seatView(),
              totalView(),
              buttonView(),
            ],
          ),
        ),
      ),
    );
  }
}
