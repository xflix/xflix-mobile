import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_bloc/bloc/seat/seat_cubit.dart';
import 'package:movie_bloc/ui/widget/horizontal_information.dart';
import 'package:movie_bloc/util/theme.dart';

class TotalView extends StatelessWidget {
  const TotalView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SeatCubit, List<String>>(
      builder: (context, state) {
        return Container(
          margin: EdgeInsets.only(
            right: defaultMargin,
            left: defaultMargin,
            bottom: 20,
          ),
          child: Column(
            children: [
              HoizontalInformation(
                title: 'Seats',
                value: state.join(', '),
              ),
              HoizontalInformation(
                title: 'Total Seats',
                value: state.length.toString(),
              ),
            ],
          ),
        );
      },
    );
  }
}
