import 'package:flutter/material.dart';
import 'package:movie_bloc/bloc/order/order_bloc.dart';
import 'package:movie_bloc/model/movie_model.dart';
import 'package:movie_bloc/ui/page/detail/header.dart';
import 'package:movie_bloc/ui/page/location/location_page.dart';
import 'package:movie_bloc/ui/widget/custom_button.dart';
import 'package:movie_bloc/ui/widget/rating_card.dart';
import 'package:movie_bloc/util/theme.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class DetailPage extends StatelessWidget {
  final MovieModel movie;
  const DetailPage({Key? key, required this.movie}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget header() {
      return HeaderDetail(movie: movie);
    }

    Widget body() {
      return Container(
        margin: EdgeInsets.symmetric(horizontal: 15, vertical: 15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              movie.overview!,
              style: whiteTextStyle.copyWith(
                fontSize: 14,
                fontWeight: medium,
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                RatingCard(
                  movie: movie,
                  title: 'Rate',
                ),
                RatingCard(
                  movie: movie,
                  title: 'Vote',
                ),
                RatingCard(
                  movie: movie,
                  title: 'Count',
                ),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            Text(
              'Release Date : ${movie.release}',
              style: whiteTextStyle.copyWith(
                fontSize: 14,
                fontWeight: light,
              ),
            )
          ],
        ),
      );
    }

    Widget buttonOrder() {
      return BlocConsumer<OrderBloc, OrderState>(
        listener: (context, state) {
          if (state is OrderLoading) {
          } else if (state is OrderSuccess) {
            Navigator.pushNamed(context, LocationPage.id);
          } else if (state is OrderFailed) {
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              content: Text(state.error),
              backgroundColor: kPinkColor,
            ));
          }
        },
        builder: (context, state) {
          return CustomButton(
            name: 'Order Tickets',
            onpress: () {
              context.read<OrderBloc>().add(AddOrderMovie(movie));
            },
          );
        },
      );
    }

    return Scaffold(
      backgroundColor: kPrimaryColor,
      body: ListView(
        children: [
          header(),
          body(),
          buttonOrder(),
        ],
      ),
    );
  }
}
