import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:movie_bloc/model/movie_model.dart';
import 'package:movie_bloc/util/const.dart';
import 'package:movie_bloc/util/theme.dart';

class HeaderDetail extends StatelessWidget {
  final MovieModel movie;
  const HeaderDetail({
    Key? key,
    required this.movie,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 350,
      decoration: BoxDecoration(
        color: kPrimaryColor,
        image: DecorationImage(
          fit: BoxFit.cover,
          image: NetworkImage(posterUrl + movie.posterPath!),
        ),
      ),
      child: Stack(
        children: [
          //NOTE:Button Back
          Align(
            alignment: Alignment.topLeft,
            child: GestureDetector(
              onTap: () {
                Navigator.pop(context);
              },
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Icon(
                  FontAwesomeIcons.arrowLeft,
                  color: kWhiteColor,
                ),
              ),
            ),
          ),
          //NOTE:Shadow
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              height: 300,
              width: double.infinity,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    kWhiteColor.withOpacity(0),
                    kPrimaryColor,
                  ],
                ),
              ),
            ),
          ),
          //NOTE:Text
          Align(
            alignment: Alignment.bottomLeft,
            child: Padding(
              padding: EdgeInsets.all(15),
              child: movie.originalTitle != null
                  ? Text(
                      movie.originalTitle!,
                      style: whiteTextStyle.copyWith(
                        fontWeight: semiBold,
                        fontSize: 24,
                      ),
                    )
                  : Text(
                      'Loading...',
                      style: whiteTextStyle.copyWith(
                        fontWeight: semiBold,
                        fontSize: 18,
                      ),
                    ),
            ),
          ),
        ],
      ),
    );
  }
}
