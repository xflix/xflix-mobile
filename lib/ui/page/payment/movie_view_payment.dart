import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_bloc/bloc/order/order_bloc.dart';
import 'package:movie_bloc/ui/widget/card_movie_payment.dart';

class MovieViewPayment extends StatelessWidget {
  const MovieViewPayment({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<OrderBloc, OrderState>(
      builder: (context, state) {
        if (state is OrderSuccess) {
          return CardMoviePayment(
            movie: state.movie,
          );
        } else {
          return SizedBox();
        }
      },
    );
  }
}
