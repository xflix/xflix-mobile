import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_bloc/bloc/location_order/location_order_cubit.dart';
import 'package:movie_bloc/ui/widget/card_location.dart';

class LocationViewPayment extends StatelessWidget {
  const LocationViewPayment({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LocationOrderCubit, LocationOrderState>(
      builder: (context, state) {
        if (state is OrderLocationSuccess) {
          return Container(
            child: CardLocation(
              location: state.location,
              ontap: () {},
            ),
          );
        } else {
          return SizedBox();
        }
      },
    );
  }
}
