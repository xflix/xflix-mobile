import 'package:flutter/material.dart';
import 'package:movie_bloc/bloc/schedule_order/schedule_order_cubit.dart';
import 'package:movie_bloc/bloc/seat/seat_cubit.dart';
import 'package:movie_bloc/model/schedule_model.dart';
import 'package:movie_bloc/ui/widget/horizontal_information.dart';
import 'package:movie_bloc/util/theme.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class DetailPaymentView extends StatelessWidget {
  const DetailPaymentView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ScheduleModel scheduleModel =
        context.select<ScheduleOrderCubit, ScheduleModel>((value) {
      if (value.state is ScheduleOrderSuccess) {
        return (value.state as ScheduleOrderSuccess).scheduleModel;
      } else {
        return ScheduleModel(
            day: 'day', month: 'month', year: 'year', time: 'time');
      }
    });
    return Column(
      children: [
        HoizontalInformation(
          title: 'Day',
          value:
              '${scheduleModel.day} / ${scheduleModel.month} / ${scheduleModel.year}',
          titleColor: kPrimaryColor,
          color: kPrimaryColor,
        ),
        HoizontalInformation(
          title: 'Time',
          value: '${scheduleModel.time} PM',
          titleColor: kPrimaryColor,
          color: kPrimaryColor,
        ),
        HoizontalInformation(
          title: 'Studio',
          value: '5',
          titleColor: kPrimaryColor,
          color: kPrimaryColor,
        ),
        HoizontalInformation(
          title: 'Seats',
          value: context.watch<SeatCubit>().state.join(', '),
          titleColor: kPrimaryColor,
          color: kPrimaryColor,
        ),
        HoizontalInformation(
          title: 'Total Seats',
          value: context.watch<SeatCubit>().state.length.toString(),
          titleColor: kPrimaryColor,
          color: kPrimaryColor,
        ),
        HoizontalInformation(
          title: 'Price',
          value: 'Rp.50.000,-',
          titleColor: kPrimaryColor,
          color: kPrimaryColor,
        ),
        Divider(
          height: 2,
          thickness: 2,
          color: kGreyColor,
        ),
        SizedBox(
          height: 10,
        ),
        HoizontalInformation(
          title: 'Tax',
          value: '10%',
          titleColor: kPrimaryColor,
          color: kPinkColor,
        ),
        HoizontalInformation(
          title: 'Total Price',
          value:
              'Rp. ${(context.watch<SeatCubit>().state.length * 50000) * 0.1 + (context.watch<SeatCubit>().state.length * 50000)}',
          titleColor: kPrimaryColor,
          color: kGreenColor,
        ),
      ],
    );
  }
}
