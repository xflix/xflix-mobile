import 'package:flutter/material.dart';
import 'package:movie_bloc/ui/page/payment/detail_payment_view.dart';
import 'package:movie_bloc/ui/page/payment/location_view_payment.dart';
import 'package:movie_bloc/ui/page/payment/movie_view_payment.dart';
import 'package:movie_bloc/ui/widget/card_point.dart';
import 'package:movie_bloc/ui/widget/custom_button.dart';
import 'package:movie_bloc/util/theme.dart';

class PaymentPage extends StatelessWidget {
  static const String id = "payment_page";
  const PaymentPage({Key? key}) : super(key: key);
  Widget headerView() {
    return Container(
      margin: EdgeInsets.only(
        left: defaultMargin,
        right: defaultMargin,
        top: 30,
        bottom: 10,
      ),
      child: Text(
        'Payment',
        style: whiteTextStyle.copyWith(
          fontSize: 24,
          fontWeight: semiBold,
        ),
      ),
    );
  }

  Widget movieViewPayment() {
    return MovieViewPayment();
  }

  Widget locationViewPayment() {
    return LocationViewPayment();
  }

  Widget detailPaymentView() {
    return DetailPaymentView();
  }

  Widget cardPaymentView() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 18),
      margin: EdgeInsets.symmetric(horizontal: defaultMargin, vertical: 15),
      decoration: BoxDecoration(
        color: kWhiteColor,
        borderRadius: BorderRadius.circular(15),
        boxShadow: [
          BoxShadow(
            color: Colors.grey,
            offset: Offset(0.0, 1.0), //(x,y)
            blurRadius: 1.0,
          ),
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'XFlix',
            style: darkTextStyle.copyWith(fontWeight: semiBold, fontSize: 18),
          ),
          SizedBox(
            height: 10,
          ),
          movieViewPayment(),
          locationViewPayment(),
          SizedBox(
            height: 15,
          ),
          Text(
            'Payment Details',
            style: darkTextStyle.copyWith(fontWeight: semiBold, fontSize: 18),
          ),
          SizedBox(
            height: 10,
          ),
          detailPaymentView()
        ],
      ),
    );
  }

  Widget paymentMethod() {
    return PointCard();
  }

  Widget buttonPay() {
    return CustomButton(onpress: () {}, name: "Pay");
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: kPrimaryColor,
        body: SingleChildScrollView(
          child: Column(
            children: [
              headerView(),
              cardPaymentView(),
              paymentMethod(),
              buttonPay(),
            ],
          ),
        ),
      ),
    );
  }
}
