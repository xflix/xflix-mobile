import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_bloc/bloc/location/location_bloc.dart';
import 'package:movie_bloc/bloc/location_order/location_order_cubit.dart';
import 'package:movie_bloc/bloc/order/order_bloc.dart';
import 'package:movie_bloc/ui/page/order/order_page.dart';
import 'package:movie_bloc/ui/widget/card_location.dart';
import 'package:movie_bloc/ui/widget/custom_button.dart';
import 'package:movie_bloc/util/theme.dart';

class LocationPage extends StatefulWidget {
  static const String id = "location_page";
  const LocationPage({Key? key}) : super(key: key);

  @override
  _LocationPageState createState() => _LocationPageState();
}

class _LocationPageState extends State<LocationPage> {
  //NOTE:init state
  @override
  void initState() {
    context.read<LocationBloc>().add(GetLocation());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    //NOTE:BODY
    Widget body() {
      return BlocBuilder<LocationBloc, LocationState>(
        builder: (context, state) {
          //NOTE:if state loading
          if (state is LocationLoading) {
            return Expanded(
              child: Center(
                child: CircularProgressIndicator(),
              ),
            );
          }
          //NOTE:Success get Api Location
          else if (state is LocationSuccess) {
            return Expanded(
              child: Container(
                margin: EdgeInsets.symmetric(horizontal: defaultMargin),
                child: ListView.builder(
                  itemCount: state.locations.length,
                  itemBuilder: (context, index) {
                    return CardLocation(
                      ontap: () {
                        context
                            .read<OrderBloc>()
                            .add(OrderGetAllCheckoutEvent());
                        context
                            .read<LocationOrderCubit>()
                            .chooseLocation(state.locations[index]);
                      },
                      location: state.locations[index],
                    );
                  },
                ),
              ),
            );
          }
          //NOTE:Failed Get Api Location
          else if (state is LocationFailed) {
            print("MY ERRORR ${state.error}");
            return SizedBox();
          } else {
            return SizedBox();
          }
        },
      );
    }

    Widget buttonChooseSchedule() {
      return CustomButton(
        onpress: () {
          Navigator.pushNamed(context, OrderPage.id);
        },
        name: "Choose Location",
      );
    }

    return Scaffold(
      backgroundColor: kPrimaryColor,
      appBar: AppBar(
        title: Text(
          'Choose Location',
          style: whiteTextStyle,
        ),
        backgroundColor: kPrimaryColor,
        elevation: 0,
      ),
      body: Column(
        children: [
          body(),
          buttonChooseSchedule(),
        ],
      ),
    );
  }
}
