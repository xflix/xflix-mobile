import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:movie_bloc/bloc/Page/page_cubit.dart';
import 'package:movie_bloc/ui/page/home/home_page.dart';
import 'package:movie_bloc/ui/page/profile/profile_page.dart';
import 'package:movie_bloc/ui/page/transaction/transaction_page.dart';
import 'package:movie_bloc/ui/page/wallet/wallet_page.dart';
import 'package:movie_bloc/util/theme.dart';

class MainPage extends StatelessWidget {
  static const String id = 'main_page';

  @override
  Widget build(BuildContext context) {
    Widget buildContent(int currentIndex) {
      switch (currentIndex) {
        case 0:
          return HomePage();
        case 1:
          return WalletPage();
        case 2:
          return TransactionPage();
        case 3:
          return ProfilePage();
        default:
          return HomePage();
      }
    }

    return BlocBuilder<PageCubit, int>(
      builder: (context, state) {
        return Scaffold(
          backgroundColor: kPrimaryColor,
          bottomNavigationBar: BottomNavigationBar(
            backgroundColor: kDarkColor,
            currentIndex: state,
            onTap: (index) {
              context.read<PageCubit>().setPage(index);
            },
            elevation: 2,
            selectedItemColor: kWhiteColor,
            selectedLabelStyle: whiteTextStyle.copyWith(fontWeight: medium),
            unselectedItemColor: kGreyColor,
            showUnselectedLabels: false,
            type: BottomNavigationBarType.fixed,
            items: [
              BottomNavigationBarItem(
                icon: Icon(FontAwesomeIcons.home),
                label: 'Home',
              ),
              BottomNavigationBarItem(
                icon: Icon(FontAwesomeIcons.wallet),
                label: 'Wallet',
              ),
              BottomNavigationBarItem(
                icon: Icon(FontAwesomeIcons.moneyBill),
                label: 'Transaction',
              ),
              BottomNavigationBarItem(
                icon: Icon(FontAwesomeIcons.userAlt),
                label: 'Profile',
              ),
            ],
          ),
          body: buildContent(state),
        );
      },
    );
  }
}
