import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_bloc/bloc/order/order_bloc.dart';
import 'package:movie_bloc/model/movie_model.dart';
import 'package:movie_bloc/ui/page/order/content.dart';
import 'package:movie_bloc/ui/page/order/header.dart';
import 'package:movie_bloc/util/theme.dart';
import 'package:movie_bloc/util/time.dart';

class OrderPage extends StatelessWidget {
  static const String id = 'order_page';
  const OrderPage({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final List<String> listTime = Time().getTime();

    Widget header(MovieModel movie) {
      return HeaderOrder(movie: movie);
    }

    Widget content(MovieModel movie) {
      return ContentOrder(movie: movie, listTime: listTime);
    }

    return BlocBuilder<OrderBloc, OrderState>(
      builder: (context, state) {
        if (state is OrderSuccess) {
          return Scaffold(
            backgroundColor: kPrimaryColor,
            body: Stack(
              children: [
                header(state.movie),
                content(state.movie),
              ],
            ),
          );
        } else {
          return Scaffold();
        }
      },
    );
  }
}
