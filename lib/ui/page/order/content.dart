import 'package:date_picker_timeline/date_picker_timeline.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:movie_bloc/bloc/schedule_order/schedule_order_cubit.dart';
import 'package:movie_bloc/model/movie_model.dart';
import 'package:movie_bloc/model/schedule_model.dart';
import 'package:movie_bloc/ui/page/seat/seat_page.dart';
import 'package:movie_bloc/ui/widget/custom_button.dart';
import 'package:movie_bloc/ui/widget/time_card.dart';
import 'package:movie_bloc/util/theme.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ContentOrder extends StatelessWidget {
  const ContentOrder({
    Key? key,
    required this.movie,
    required this.listTime,
  }) : super(key: key);

  final MovieModel movie;
  final List<String> listTime;

  @override
  Widget build(BuildContext context) {
    String day = "";
    String month = "";
    String year = "";
    String time = "";
    return SingleChildScrollView(
      child: Container(
        margin: EdgeInsets.only(top: 200),
        padding: EdgeInsets.only(left: 15, right: 15, top: 15),
        width: double.infinity,
        decoration: BoxDecoration(
          color: kPrimaryColor,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(30),
            topRight: Radius.circular(30),
          ),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            GestureDetector(
              onTap: () {
                Navigator.pop(context);
              },
              child: Icon(
                FontAwesomeIcons.arrowLeft,
                color: kWhiteColor,
                size: 20,
              ),
            ),
            SizedBox(
              height: 15,
            ),
            //NOTE:name of Movie
            Text(
              movie.originalTitle!,
              style: whiteTextStyle.copyWith(fontWeight: bold, fontSize: 20),
            ),
            SizedBox(
              height: 15,
            ),
            //NOTE:Calendar Movie
            Column(
              children: [
                DatePicker(
                  DateTime.now(),
                  initialSelectedDate: DateTime.now(),
                  selectionColor: kPinkColor,
                  monthTextStyle: TextStyle(color: kWhiteColor),
                  dateTextStyle: TextStyle(color: kWhiteColor),
                  dayTextStyle: TextStyle(color: kWhiteColor),
                  selectedTextColor: kWhiteColor,
                  onDateChange: (date) {
                    day = date.day.toString();
                    month = date.month.toString();
                    year = date.year.toString();
                  },
                ),
              ],
            ),

            //NOTE:List Time
            Container(
              alignment: Alignment.topCenter,
              child: GridView.count(
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                crossAxisCount: 4,
                children: List.generate(
                  listTime.length,
                  (index) {
                    return TimeCard(
                      name: listTime[index],
                      ontap: () {
                        context
                            .read<ScheduleOrderCubit>()
                            .setTime(listTime[index]);
                        time = listTime[index].toString();
                      },
                    );
                  },
                ),
              ),
            ),
            //NOTE:Order Button
            CustomButton(
              onpress: () {
                ScheduleModel scheduleModel = ScheduleModel(
                    day: day, month: month, year: year, time: time);
                context.read<ScheduleOrderCubit>().setSchedule(scheduleModel);
                Navigator.pushNamed(context, SeatPage.id);
              },
              name: 'Choose Schedule',
            )
          ],
        ),
      ),
    );
  }
}
