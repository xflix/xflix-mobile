import 'package:flutter/material.dart';
import 'package:movie_bloc/model/movie_model.dart';
import 'package:movie_bloc/util/const.dart';
import 'package:movie_bloc/util/theme.dart';

class HeaderOrder extends StatelessWidget {
  const HeaderOrder({
    Key? key,
    required this.movie,
  }) : super(key: key);

  final MovieModel movie;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 300,
      decoration: BoxDecoration(
        color: kPrimaryColor,
        image: DecorationImage(
            fit: BoxFit.cover,
            image: NetworkImage(posterUrl + movie.posterPath!)),
      ),
    );
  }
}
