import 'package:flutter/material.dart';
import 'package:movie_bloc/bloc/location_order/location_order_cubit.dart';
import 'package:movie_bloc/model/location_model.dart';
import 'package:movie_bloc/util/theme.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CardLocation extends StatelessWidget {
  final LocationModel location;
  final GestureTapCallback ontap;
  final Color color;
  const CardLocation({
    required this.location,
    required this.ontap,
    this.color = kGreyColor,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Color isSelected = context.select<LocationOrderCubit, Color>((value) {
      if (value.state is OrderLocationSuccess) {
        if ((value.state as OrderLocationSuccess).location.id == location.id) {
          return kWhiteColor;
        } else {
          return kGreyColor;
        }
      } else {
        return kGreyColor;
      }
    });
    return GestureDetector(
      onTap: ontap,
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 10.0),
        decoration: BoxDecoration(
          color: isSelected,
          borderRadius: BorderRadius.circular(10),
        ),
        child: Row(
          children: [
            Container(
              height: 70,
              width: 70,
              margin: EdgeInsets.all(5),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                image: DecorationImage(
                  fit: BoxFit.fill,
                  image: AssetImage('assets/xx1.png'),
                ),
              ),
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    location.name!,
                    overflow: TextOverflow.ellipsis,
                    style: darkTextStyle.copyWith(
                      fontWeight: semiBold,
                      fontSize: 16,
                    ),
                  ),
                  Text(
                    location.place!,
                    overflow: TextOverflow.ellipsis,
                    style: darkTextStyle.copyWith(
                      fontWeight: medium,
                      fontSize: 16,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
