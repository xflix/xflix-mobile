import 'package:flutter/material.dart';
import 'package:movie_bloc/model/movie_model.dart';
import 'package:movie_bloc/util/const.dart';
import 'package:movie_bloc/util/theme.dart';

class CardMoviePayment extends StatelessWidget {
  final MovieModel movie;
  const CardMoviePayment({Key? key, required this.movie}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: kWhiteColor,
        borderRadius: BorderRadius.circular(10),
      ),
      child: Row(
        children: [
          Container(
            height: 70,
            width: 70,
            margin: EdgeInsets.all(5),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              image: DecorationImage(
                fit: BoxFit.cover,
                image: NetworkImage(posterUrl + movie.posterPath!),
              ),
            ),
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  movie.originalTitle!,
                  overflow: TextOverflow.ellipsis,
                  style: darkTextStyle.copyWith(
                    fontWeight: semiBold,
                    fontSize: 16,
                  ),
                ),
                Text(
                  movie.originalLanguage!,
                  overflow: TextOverflow.ellipsis,
                  style: darkTextStyle.copyWith(
                    fontWeight: light,
                    fontSize: 16,
                  ),
                ),
              ],
            ),
          ),
          Row(
            children: [
              Container(
                height: 20,
                width: 20,
                padding: EdgeInsets.all(6),
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('assets/icon_star.png'),
                  ),
                ),
              ),
              SizedBox(
                width: 5,
              ),
              Text(
                movie.voteAverage.toString(),
                style: darkTextStyle.copyWith(
                  fontSize: 16,
                  fontWeight: semiBold,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
