import 'package:flutter/material.dart';
import 'package:movie_bloc/bloc/schedule_order/schedule_order_cubit.dart';
import 'package:movie_bloc/util/theme.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class TimeCard extends StatelessWidget {
  final String name;
  final GestureTapCallback ontap;
  const TimeCard({
    Key? key,
    required this.name,
    required this.ontap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Color isSelected = context.select<ScheduleOrderCubit, Color>((value) {
      if (value.state is ScheduleTimeOrderSuccess) {
        if ((value.state as ScheduleTimeOrderSuccess).time == name) {
          return kWhiteColor;
        } else {
          return kGreyColor;
        }
      } else {
        return kGreyColor;
      }
    });
    return GestureDetector(
      onTap: ontap,
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          border: Border.all(color: isSelected),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              '$name PM',
              textAlign: TextAlign.center,
              style: greyTextStyle.copyWith(
                fontSize: 16,
                fontWeight: medium,
              ),
            )
          ],
        ),
      ),
    );
  }
}
