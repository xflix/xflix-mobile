import 'package:flutter/material.dart';
import 'package:movie_bloc/model/movie_model.dart';
import 'package:movie_bloc/util/theme.dart';

class RatingCard extends StatelessWidget {
  const RatingCard({Key? key, required this.movie, this.title = ''})
      : super(key: key);

  final MovieModel movie;
  final String title;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(
          title,
          style: whiteTextStyle.copyWith(
            fontSize: 16,
            fontWeight: semiBold,
          ),
        ),
        SizedBox(
          height: 5,
        ),
        Container(
          width: 20,
          height: 20,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/icon_star.png'),
            ),
          ),
        ),
        Text(
          movie.voteAverage.toString(),
          style: whiteTextStyle.copyWith(
            fontWeight: semiBold,
            fontSize: 16,
          ),
        )
      ],
    );
  }
}
