import 'package:flutter/material.dart';
import 'package:movie_bloc/bloc/seat/seat_cubit.dart';
import 'package:movie_bloc/util/theme.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CardSeat extends StatelessWidget {
  final String id;
  final bool isAvailable;
  const CardSeat({
    Key? key,
    required this.id,
    this.isAvailable = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    bool isSelected = context.watch<SeatCubit>().isSelected(id);
    Color backgroundColor() {
      if (!isAvailable) {
        return kPinkColor;
      } else {
        if (isSelected) {
          return kGreenColor;
        } else {
          return kGreyColor;
        }
      }
    }

    return GestureDetector(
      onTap: () {
        if (isAvailable) {
          context.read<SeatCubit>().selectSeat(id);
        }
      },
      child: Container(
        margin: EdgeInsets.all(5),
        decoration: BoxDecoration(
          color: backgroundColor(),
          borderRadius: BorderRadius.circular(10),
          border: Border.all(
            color: kWhiteColor,
          ),
        ),
      ),
    );
  }
}
