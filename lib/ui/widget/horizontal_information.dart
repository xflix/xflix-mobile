import 'package:flutter/material.dart';
import 'package:movie_bloc/util/theme.dart';

class HoizontalInformation extends StatelessWidget {
  final String title;
  final String value;
  final Color titleColor;
  final Color color;
  HoizontalInformation(
      {Key? key,
      required this.title,
      required this.value,
      this.titleColor = kWhiteColor,
      this.color = kWhiteColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: Text(
              title,
              style: whiteTextStyle.copyWith(
                color: titleColor,
                fontSize: 16,
                fontWeight: medium,
              ),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              value,
              textAlign: TextAlign.end,
              style: whiteTextStyle.copyWith(
                color: color,
                fontSize: 16,
                fontWeight: semiBold,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
