import 'package:flutter/material.dart';
import 'package:movie_bloc/model/movie_model.dart';
import 'package:movie_bloc/ui/page/detail/detail_page.dart';
import 'package:movie_bloc/util/const.dart';
import 'package:movie_bloc/util/theme.dart';

class HorizontalMovieCard extends StatelessWidget {
  final MovieModel movie;
  final double height;
  const HorizontalMovieCard({
    Key? key,
    required this.movie,
    required this.height,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => DetailPage(movie: movie)));
      },
      child: Container(
        height: height,
        margin: EdgeInsets.symmetric(horizontal: 5),
        width: 350,
        decoration: BoxDecoration(
          image: movie.backdropPath != null
              ? DecorationImage(
                  fit: BoxFit.cover,
                  image: NetworkImage(posterUrl + movie.backdropPath!),
                )
              : DecorationImage(
                  image: AssetImage('assets/image_destination1.png'),
                ),
          borderRadius: BorderRadius.circular(18),
          color: kGreyColor,
        ),
        child: Stack(
          children: [
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                height: 80,
                width: double.infinity,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(18),
                  gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                      kWhiteColor.withOpacity(0),
                      Colors.black.withOpacity(0.50),
                    ],
                  ),
                ),
              ),
            ),
            Align(
              alignment: Alignment.bottomLeft,
              child: Padding(
                padding: EdgeInsets.all(defaultMargin),
                child: movie.originalTitle != null
                    ? Text(
                        movie.originalTitle!,
                        style: whiteTextStyle.copyWith(
                          fontWeight: semiBold,
                          fontSize: 18,
                        ),
                      )
                    : Text(
                        'Loading...',
                        style: whiteTextStyle.copyWith(
                          fontWeight: semiBold,
                          fontSize: 18,
                        ),
                      ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
