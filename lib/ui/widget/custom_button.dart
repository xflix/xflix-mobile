import 'package:flutter/material.dart';
import 'package:movie_bloc/util/theme.dart';

class CustomButton extends StatelessWidget {
  final VoidCallback onpress;
  final String name;
  const CustomButton({Key? key, required this.onpress, required this.name})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      width: double.infinity,
      margin: EdgeInsets.symmetric(
        vertical: 10,
        horizontal: 15,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        color: kPinkColor,
      ),
      child: TextButton(
        onPressed: onpress,
        child: Text(
          name,
          style: whiteTextStyle.copyWith(
            fontWeight: semiBold,
            fontSize: 18,
          ),
        ),
      ),
    );
  }
}
