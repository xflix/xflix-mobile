import 'package:flutter/material.dart';
import 'package:movie_bloc/util/theme.dart';

class PointCard extends StatelessWidget {
  const PointCard({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: kWhiteColor,
        borderRadius: BorderRadius.circular(15),
      ),
      width: double.infinity,
      margin: EdgeInsets.only(
        top: 15,
        left: defaultMargin,
        right: defaultMargin,
        bottom: 30,
      ),
      padding: EdgeInsets.symmetric(
        horizontal: 15,
        vertical: 20,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Points',
            style: darkTextStyle.copyWith(fontSize: 18, fontWeight: bold),
          ),
          SizedBox(
            height: 8,
          ),
          Row(
            children: [
              Container(
                height: 70,
                width: 100,
                margin: EdgeInsets.only(right: 16),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  gradient: LinearGradient(
                    colors: [
                      kPrimaryColor,
                      kPinkColor,
                    ],
                    begin: const FractionalOffset(0.0, 0.0),
                    end: const FractionalOffset(1.0, 0.0),
                  ),
                ),
                child: Align(
                  alignment: Alignment.center,
                  child: Text(
                    'XFLIX',
                    style: whiteTextStyle.copyWith(
                      fontWeight: semiBold,
                      fontSize: 18,
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'XP 30.000,-',
                      style: darkTextStyle.copyWith(
                          fontSize: 18, fontWeight: semiBold),
                    ),
                    Text(
                      'Current Points',
                      style: darkTextStyle.copyWith(
                          fontSize: 16, fontWeight: medium),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
