import 'package:flutter/material.dart';
import 'package:movie_bloc/util/theme.dart';

class StatusItem extends StatelessWidget {
  final String name;
  final Color color;
  const StatusItem({Key? key, required this.name, required this.color})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Row(
        children: [
          Container(
            height: 20,
            width: 20,
            decoration: BoxDecoration(
              color: color,
              borderRadius: BorderRadius.circular(5),
              border: Border.all(
                color: kWhiteColor,
              ),
            ),
          ),
          SizedBox(
            width: 10,
          ),
          Expanded(
            child: Text(
              name,
              overflow: TextOverflow.clip,
              maxLines: 1,
              style: whiteTextStyle.copyWith(fontWeight: medium, fontSize: 16),
            ),
          )
        ],
      ),
    );
  }
}
