import 'package:flutter/material.dart';
import 'package:movie_bloc/model/movie_model.dart';
import 'package:movie_bloc/ui/page/detail/detail_page.dart';
import 'package:movie_bloc/util/const.dart';
import 'package:movie_bloc/util/theme.dart';

class CardMovie extends StatelessWidget {
  final MovieModel movie;
  const CardMovie({
    Key? key,
    required this.movie,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => DetailPage(movie: movie),
          ),
        );
      },
      child: Container(
        height: 250,
        width: 150,
        margin: EdgeInsets.only(left: defaultMargin),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: 200,
              width: 150,
              padding: EdgeInsets.all(8),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: kPrimaryColor,
                image: DecorationImage(
                  fit: BoxFit.cover,
                  image: NetworkImage(posterUrl + movie.posterPath!),
                ),
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                movie.originalTitle != null
                    ? Text(
                        movie.originalTitle!,
                        overflow: TextOverflow.ellipsis,
                        style: darkTextStyle.copyWith(
                          fontSize: 16,
                          fontWeight: semiBold,
                        ),
                      )
                    : Text(
                        'Loading...',
                        overflow: TextOverflow.ellipsis,
                        style: darkTextStyle.copyWith(
                          fontSize: 16,
                          fontWeight: semiBold,
                        ),
                      ),
                Row(
                  children: [
                    Container(
                      height: 20,
                      width: 20,
                      padding: EdgeInsets.all(6),
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage('assets/icon_star.png'),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Text(
                      movie.voteAverage.toString(),
                      style: darkTextStyle.copyWith(
                        fontSize: 16,
                        fontWeight: semiBold,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
