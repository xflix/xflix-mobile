import 'dart:convert';

import 'package:movie_bloc/model/movie_model.dart';
import 'package:movie_bloc/services/network_service.dart';
import 'package:movie_bloc/util/const.dart';

class MovieService {
  final String key = apiKey;
  final String base = baseUrl;

  Future<List<MovieModel>> getUpcomingMovie() async {
    try {
      String endPoint = "/movie/upcoming";
      String url = base + endPoint + '?api_key=' + apiKey;
      NetworkService network = NetworkService(url: url, apikey: key);

      var response = await network.getDataApi();
      final responseDecode = jsonDecode(response);
      List<MovieModel> upcomingMovies = [];

      for (var results in responseDecode['results']) {
        if (results != null) {
          upcomingMovies.add(MovieModel.fromjson(results));
        }
      }

      return upcomingMovies;
    } catch (e) {
      throw e;
    }
  }

  Future<List<MovieModel>> getNowPlaying() async {
    try {
      String endPoint = "/movie/now_playing";
      String url = base + endPoint + '?api_key=' + apiKey;
      NetworkService network = NetworkService(url: url, apikey: key);

      var response = await network.getDataApi();
      final responseDecode = jsonDecode(response);
      List<MovieModel> upcomingMovies = [];

      for (var results in responseDecode['results']) {
        if (results != null) {
          upcomingMovies.add(MovieModel.fromjson(results));
        }
      }

      return upcomingMovies;
    } catch (e) {
      throw e;
    }
  }

  Future<List<MovieModel>> fetchDataMovie() async {
    try {
      String endPoint = "/trending/all/day";
      String url = base + endPoint + '?api_key=' + apiKey;
      NetworkService network = NetworkService(url: url, apikey: key);
      //NOTE:get data
      var response = await network.getDataApi();
      final responseDecode = jsonDecode(response);
      //NOTE: create instance object
      List<MovieModel> movies = [];
      for (var results in responseDecode['results']) {
        if (results != null) {
          movies.add(MovieModel.fromjson(results));
        }
      }
      return movies;
    } catch (e) {
      throw e;
    }
  }
}
