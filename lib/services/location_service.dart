import 'dart:convert';

import 'package:movie_bloc/model/location_model.dart';
import 'package:movie_bloc/services/network_service.dart';

class LocationService {
  final endPoint =
      "https://612dbe0ee579e1001791dd53.mockapi.io/api/v1/bioskop-data";

  Future<List<LocationModel>> getLocationFromApi() async {
    try {
      NetworkService network = NetworkService(url: endPoint, apikey: '');

      var response = await network.getDataApi();
      var responseDecode = jsonDecode(response);
      List<LocationModel> locations = [];

      for (var location in responseDecode) {
        locations.add(LocationModel.fromjson(location));
      }
      return locations;
    } catch (e) {
      throw e;
    }
  }
}
