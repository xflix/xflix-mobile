import 'package:http/http.dart' as http;

class NetworkService {
  final String url;
  final String apikey;
  NetworkService({
    required this.url,
    required this.apikey,
  });

  Future<dynamic> getDataApi() async {
    try {
      var uri = Uri.parse(url);
      var response = await http.get(
        uri,
      );
      if (response.statusCode == 200) {
        return response.body;
      } else {
        print('Internal Server Error');
      }
    } catch (e) {
      throw e;
    }
  }
}
