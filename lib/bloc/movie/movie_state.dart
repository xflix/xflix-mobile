part of 'movie_bloc.dart';

abstract class MovieState extends Equatable {
  const MovieState();

  @override
  List<Object> get props => [];
}

class MovieInitial extends MovieState {}

class MovieLoading extends MovieState {
  final bool loading;
  MovieLoading(this.loading);

  @override
  List<Object> get props => [loading];
}

class MovieSuccess extends MovieState {
  final List<MovieModel> movies;
  final List<MovieModel> commingMovies;
  final List<MovieModel> nowPlayingMovies;
  MovieSuccess(this.movies, this.commingMovies, this.nowPlayingMovies);

  @override
  List<Object> get props => [movies, commingMovies, nowPlayingMovies];
}

class MovieFailed extends MovieState {
  final String error;
  MovieFailed(this.error);
  @override
  List<Object> get props => [error];
}
