import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:movie_bloc/model/movie_model.dart';
import 'package:movie_bloc/services/movie_service.dart';

part 'movie_event.dart';
part 'movie_state.dart';

class MovieBloc extends Bloc<MovieEvent, MovieState> {
  MovieBloc() : super(MovieInitial());

  @override
  Stream<MovieState> mapEventToState(
    MovieEvent event,
  ) async* {
    if (event is GetMovieTrending) {
      try {
        if (state is MovieInitial) {
          List<MovieModel> movies = await MovieService().fetchDataMovie();
          List<MovieModel> upcomingMovies =
              await MovieService().getUpcomingMovie();
          List<MovieModel> nowPlaying = await MovieService().getNowPlaying();
          yield (MovieSuccess(movies, upcomingMovies, nowPlaying));
        }
      } catch (e) {
        yield (MovieFailed(e.toString()));
      }
    }
  }
}
