import 'package:bloc/bloc.dart';

class SeatCubit extends Cubit<List<String>> {
  SeatCubit() : super([]);
  void selectSeat(String id) {
    print('Before Selected $state');
    if (!isSelected(id)) {
      state.add(id);
    } else {
      state.remove(id);
    }
    print('after Selected $state');
    emit(List.of(state));
  }

  bool isSelected(String id) {
    int index = state.indexOf(id);

    if (index == -1) {
      return false;
    } else {
      return true;
    }
  }
}
