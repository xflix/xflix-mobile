part of 'location_order_cubit.dart';

abstract class LocationOrderState extends Equatable {
  const LocationOrderState();

  @override
  List<Object> get props => [];
}

class LocationOrderInitial extends LocationOrderState {}

class OrderLocationSuccess extends LocationOrderState {
  final LocationModel location;
  OrderLocationSuccess(this.location);
  @override
  List<Object> get props => [location];
}
