import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:movie_bloc/model/location_model.dart';

part 'location_order_state.dart';

class LocationOrderCubit extends Cubit<LocationOrderState> {
  LocationOrderCubit() : super(LocationOrderInitial());

  void chooseLocation(LocationModel location) {
    emit(OrderLocationSuccess(location));
  }
}
