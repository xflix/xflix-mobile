part of 'location_bloc.dart';

abstract class LocationState extends Equatable {
  const LocationState();

  @override
  List<Object> get props => [];
}

class LocationInitial extends LocationState {}

class LocationLoading extends LocationState {}

class LocationSuccess extends LocationState {
  final List<LocationModel> locations;
  LocationSuccess(this.locations);

  @override
  List<Object> get props => locations;
}

class LocationFailed extends LocationState {
  final String error;
  LocationFailed(this.error);

  @override
  List<Object> get props => [error];
}
