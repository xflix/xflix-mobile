import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:movie_bloc/model/location_model.dart';
import 'package:movie_bloc/services/location_service.dart';

part 'location_event.dart';
part 'location_state.dart';

class LocationBloc extends Bloc<LocationEvent, LocationState> {
  LocationBloc() : super(LocationInitial());

  @override
  Stream<LocationState> mapEventToState(
    LocationEvent event,
  ) async* {
    if (event is GetLocation) {
      yield (LocationLoading());
      try {
        List<LocationModel> locations =
            await LocationService().getLocationFromApi();
        yield (LocationSuccess(locations));
      } catch (e) {
        yield (LocationFailed(e.toString()));
      }
    }
  }
}
