part of 'order_bloc.dart';

abstract class OrderEvent extends Equatable {
  const OrderEvent();

  @override
  List<Object> get props => [];
}

class AddOrderMovie extends OrderEvent {
  final MovieModel movie;
  AddOrderMovie(this.movie);
}

class OrderLocationEvent extends OrderEvent {
  final LocationModel locationModel;
  OrderLocationEvent(this.locationModel);
}

class OrderScheduleEvent extends OrderEvent {
  final ScheduleModel scheduleModel;
  OrderScheduleEvent(this.scheduleModel);
}

class OrderGetCheckoutEvent extends OrderEvent {}

class OrderGetAllCheckoutEvent extends OrderEvent {}
