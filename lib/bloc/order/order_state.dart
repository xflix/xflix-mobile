part of 'order_bloc.dart';

abstract class OrderState extends Equatable {
  const OrderState();

  @override
  List<Object> get props => [];
}

class OrderInitial extends OrderState {}

class OrderSuccess extends OrderState {
  final MovieModel movie;
  OrderSuccess(this.movie);

  @override
  List<Object> get props => [movie];
}

class OrderLoading extends OrderState {}

//NOTE:State for location
class OrderSuccessLocationState extends OrderState {}

//NOTE:State for Checkout
class OrderSuccessCheckoutState extends OrderState {
  final CheckoutModel checkout;
  OrderSuccessCheckoutState(this.checkout);
  @override
  List<Object> get props => [checkout];
}

class OrderFailed extends OrderState {
  final String error;
  OrderFailed(this.error);

  @override
  List<Object> get props => [error];
}
