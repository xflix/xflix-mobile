import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:movie_bloc/db/checkout_db.dart';
import 'package:movie_bloc/model/checkout_model.dart';
import 'package:movie_bloc/model/location_model.dart';
import 'package:movie_bloc/model/movie_model.dart';
import 'package:movie_bloc/model/schedule_model.dart';

part 'order_event.dart';
part 'order_state.dart';

class OrderBloc extends Bloc<OrderEvent, OrderState> {
  final CheckoutDatabase checkoutDatabase;
  OrderBloc(this.checkoutDatabase) : super(OrderInitial());

  @override
  Stream<OrderState> mapEventToState(
    OrderEvent event,
  ) async* {
    //NOTE: Bloc For Movie
    if (event is AddOrderMovie) {
      try {
        yield (OrderLoading());
        yield (OrderSuccess(event.movie));
      } catch (e) {
        yield (OrderFailed(e.toString()));
      }
    }
    //NOTE: Bloc for Location
    if (event is OrderLocationEvent) {
      try {
        yield (OrderLoading());
        await checkoutDatabase.addLocation(event.locationModel);
        yield (OrderSuccessLocationState());
      } catch (e) {
        yield (OrderFailed(e.toString()));
      }
    }
    //NOTE:Bloc for Schedule
    if (event is OrderScheduleEvent) {}
    //NOTE:Bloc For Get Checkout From db
    if (event is OrderGetCheckoutEvent) {
      try {
        yield (OrderLoading());
        CheckoutModel checkout = await checkoutDatabase.getCheckOut();
        yield (OrderSuccessCheckoutState(checkout));
      } catch (e) {
        yield (OrderFailed(e.toString()));
      }
    }
    //NOTE: Get all CheckOuts
    if (event is OrderGetAllCheckoutEvent) {
      try {
        yield (OrderLoading());
        int cheks = await checkoutDatabase.getAllCheckOut();
        print("PANJANGNYA ADALAG $cheks");
      } catch (e) {}
    }
  }
}
