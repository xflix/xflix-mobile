import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:movie_bloc/model/schedule_model.dart';

part 'schedule_order_state.dart';

class ScheduleOrderCubit extends Cubit<ScheduleOrderState> {
  ScheduleOrderCubit() : super(ScheduleOrderInitial());

  void setSchedule(ScheduleModel scheduleModel) {
    emit(ScheduleOrderSuccess(scheduleModel));
    print(state);
  }

  void setTime(String time) {
    emit(ScheduleTimeOrderSuccess(time));
  }
}
