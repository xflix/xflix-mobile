part of 'schedule_order_cubit.dart';

abstract class ScheduleOrderState extends Equatable {
  const ScheduleOrderState();

  @override
  List<Object> get props => [];
}

class ScheduleOrderInitial extends ScheduleOrderState {}

class ScheduleOrderSuccess extends ScheduleOrderState {
  final ScheduleModel scheduleModel;
  ScheduleOrderSuccess(this.scheduleModel);
  @override
  List<Object> get props => [scheduleModel];
}

class ScheduleTimeOrderSuccess extends ScheduleOrderState {
  final String time;
  ScheduleTimeOrderSuccess(this.time);
  @override
  List<Object> get props => [time];
}
