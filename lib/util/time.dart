class Time {
  List<String> time = [
    "12:20",
    "13:00",
    "14:20",
    "15:45",
    "17:00",
    "19:15",
    "20:20",
    "21:15",
    "22:45",
    "23:00",
  ];

  List<String> getTime() {
    return time;
  }
}
