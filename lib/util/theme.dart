import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

const Color kPrimaryColor = Color(0xFF7261139);
const Color kDarkColor = Color(0xff00139);
const Color kWhiteColor = Color(0xffFFFFFF);
const Color kGreyColor = Color(0xff9698A9);
const Color kPinkColor = Colors.pink;
const Color kblueColor = Color(0xff70130180);
const Color kGreenColor = Color(0xff0EC3AE);

TextStyle whiteTextStyle = GoogleFonts.poppins(color: kWhiteColor);
TextStyle darkTextStyle = GoogleFonts.poppins(color: kPrimaryColor);
TextStyle greyTextStyle = GoogleFonts.poppins(color: kGreyColor);

const FontWeight light = FontWeight.w300;
const FontWeight regular = FontWeight.w400;
const FontWeight medium = FontWeight.w500;
const FontWeight semiBold = FontWeight.w600;
const FontWeight bold = FontWeight.w700;
const FontWeight extraBold = FontWeight.w800;
const FontWeight black = FontWeight.w900;

const double defaultMargin = 24;
