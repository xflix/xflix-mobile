import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_bloc/bloc/Page/page_cubit.dart';
import 'package:movie_bloc/bloc/location/location_bloc.dart';
import 'package:movie_bloc/bloc/location_order/location_order_cubit.dart';
import 'package:movie_bloc/bloc/movie/movie_bloc.dart';
import 'package:movie_bloc/bloc/order/order_bloc.dart';
import 'package:movie_bloc/bloc/schedule_order/schedule_order_cubit.dart';
import 'package:movie_bloc/bloc/seat/seat_cubit.dart';
import 'package:movie_bloc/db/checkout_db.dart';
import 'package:movie_bloc/model/checkout_model.dart';
import 'package:movie_bloc/model/location_model.dart';
import 'package:movie_bloc/ui/page/home/home_page.dart';
import 'package:movie_bloc/ui/page/location/location_page.dart';
import 'package:movie_bloc/ui/page/main_page.dart';
import 'package:movie_bloc/ui/page/order/order_page.dart';
import 'package:movie_bloc/ui/page/payment/payment_page.dart';
import 'package:movie_bloc/ui/page/profile/profile_page.dart';
import 'package:movie_bloc/ui/page/seat/seat_page.dart';
import 'package:movie_bloc/ui/page/transaction/transaction_page.dart';
import 'package:movie_bloc/ui/page/wallet/wallet_page.dart';
import 'package:path_provider/path_provider.dart' as pathProvider;
import 'package:hive/hive.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  var appDocumentDirectory =
      await pathProvider.getApplicationDocumentsDirectory();
  Hive.registerAdapter(CheckoutDB());
  Hive.registerAdapter(LocationDB());
  Hive.init(appDocumentDirectory.path);

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (context) => PageCubit()),
        BlocProvider(create: (context) => MovieBloc()),
        BlocProvider(create: (context) => LocationBloc()),
        BlocProvider(create: (context) => OrderBloc(CheckoutDatabase())),
        BlocProvider(create: (context) => LocationOrderCubit()),
        BlocProvider(create: (context) => ScheduleOrderCubit()),
        BlocProvider(create: (context) => SeatCubit()),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        home: MainPage(),
        routes: {
          MainPage.id: (context) => MainPage(),
          HomePage.id: (context) => HomePage(),
          WalletPage.id: (context) => WalletPage(),
          TransactionPage.id: (context) => TransactionPage(),
          ProfilePage.id: (context) => ProfilePage(),
          LocationPage.id: (context) => LocationPage(),
          OrderPage.id: (context) => OrderPage(),
          SeatPage.id: (context) => SeatPage(),
          PaymentPage.id: (context) => PaymentPage(),
        },
      ),
    );
  }
}
